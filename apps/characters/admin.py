from django.contrib import admin

from .models import Event, HPCharacter

admin.site.register(Event)
admin.site.register(HPCharacter)
